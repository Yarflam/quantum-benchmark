# Quantum Benchmark

![license](https://img.shields.io/badge/license-CC_BY--NC--SA-green.svg)
![version](https://img.shields.io/badge/app-vDev-blue.svg)

⚠ Don't use this container in production ⚠

## Abstract

Benchmarking of the libraries Cirq, Xanadu and QuTiP.
Jupyter is installed to code.

## Prerequisites

You have to install a Docker instance on your machine.

## Install

You need to clone the project:

> $> git clone https://gitlab.com/Yarflam/quantum-benchmark

And run this command lines:

> $> chmod +x init.sh
>
> $> ./init.sh start
>
> $> ./init.sh about

The bash script init.sh is composed some commands to control the application.
You can type "init.sh help" for more information.

## Authors

- Yarflam - *initial work*

## License

The project is licensed under Creative Commons (BY-NC-SA) - see the [LICENSE.md](LICENSE.md) file for details.

## Documentations

[Tools list to work with quantum states](https://quantumcomputingreport.com/resources/tools/)

> Open-Source Quantum Software Projects,
> Microsoft Quantum Development Kit,
> IBM Quantum Experience,
> Rigetti Forest and Cloud Computing Services (QCS),
> CAS-Alibaba Quantum Computing Laboratory,
> ProjectQ,
> Cirq,
> CirqProjectQ,
> PennyLane and Strawberry Fields from Xanadu,
> Qulacs,
> Bayesforge,
> Blueqat,
> Quantum Programming Studio,
> Atos/SFTC Hartree Centre – Quantum Learning as a Service (QLaaS),
> Quantum User Interface (QUI),
> Quirk,
> Qibo,
> XACC,
> Quantum++,
> Quantum Inspire,
> QuTiP: Quantum Toolbox in Python,
> OpenFermion,
> Quipper,
> QX Quantum Computing Simulator,
> Quantum Algorithm Zoo,
> ScaffCC,
> Qbsolv from D-Wave,
> Quantum Computing Playground,
> Microsoft LIQUi|>,
> Quantum in the Cloud,
> Raytheon BBN Open Source Software.
