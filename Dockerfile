FROM tensorflow/tensorflow:1.6.0-py3
MAINTAINER Yarflam

# Install standards dependencies
RUN apt-get update && apt-get install -y --no-install-recommends curl\
    wget nano apt-transport-https lsb-release ca-certificates git

# Install english pack
RUN apt-get install -y --no-install-recommends locales &&\
    locale-gen en_CA.UTF-8 &&\
    echo "environment=LANG=\"en_CA.utf8\", LC_ALL=\"en_CA.UTF-8\", LC_LANG=\"en_CA.UTF-8\"" > /etc/default/locale
RUN chmod 0755 /etc/default/locale
ENV LC_ALL=en_CA.UTF-8
ENV LANG=en_CA.UTF-8
ENV LANGUAGE=en_CA.UTF-8

# Install externals dependencies
RUN apt-get install -y --no-install-recommends python3-tk\
    texlive-latex-base latexmk

# Upgrade PIP
RUN pip install --upgrade pip

# Install Jupyter
RUN pip install jupyter

# Install Python dependencies
RUN pip install matplotlib numpy scikit-learn strawberryfields scipy cython nose

# Install Quantum Libraries
RUN pip install cirq qmlt qutip

# Application
WORKDIR /usr/src

CMD [ "jupyter", "notebook", "--allow-root", "--ip=0.0.0.0", "--port=8080" ]
