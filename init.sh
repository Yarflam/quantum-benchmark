#!/bin/sh

cmd=$1
test=false

# COMMANDS

if [ "$cmd" == 'start' ]; then
    test=true
    echo -e '@ Start the application\n'
    docker-compose -f ./docker-compose.yml up -d
fi

if [ "$cmd" == 'build' ]; then
    test=true
    echo -e '@ Build the application\n'
    docker-compose -f ./docker-compose.yml up -d --build
fi

if [ "$cmd" == 'about' ]; then
    test=true
    echo -e '@ Get information about the container\n'
    docker ps -a | grep "qubench"
    # Get the url
    url=$(docker logs --details qubench | grep "NotebookApp" | grep " http")
    if [ "$url" ]; then
        ip=$(docker-machine ip 2> /dev/null)
        if [ "$ip" ]; then
            url=$(echo "$url" | sed -E "s/(http:\/\/)(\\([^)]+\\)|0\.0\.0\.0):8080/\1$ip/")
        else
            url=$(echo "$url" | sed -E "s/(http:\/\/)(\\([^)]+\\)|0\.0\.0\.0):8080/\1\2/")
        fi
        echo -e "\nPlease to open the link in your browser:\n$url"
    else
        echo -e '\n@ Something is wrong\n'
        docker logs --details qubench
    fi

fi

if [ "$cmd" == 'stop' ]; then
    test=true
    echo -e '@ Stop the application'
    docker rm -f qubench
fi

if [ "$cmd" = 'docker-cleaner' ]; then
    test=true
    echo -e '@ Clean the docker environment\n'
    echo '@ Before'
    docker images
    echo -e '\n@ Action'
    docker rmi $(docker images --filter "dangling=true" -q) 2> /dev/null
    echo -e '\n@ After'
    docker images
fi

if [ "$cmd" == 'help' ]; then
    test=true
    echo -e 'Usage: init.sh [COMMAND]\n'
    echo 'Commands:'
    echo -e 'start\t\tstart the application'
    echo -e 'buid\t\tbuild and start the application'
    echo -e 'about\t\tget information about the container'
    echo -e 'stop\t\tstop the application'
    echo -e 'docker-cleaner\tclean up the docker environment'
fi

if [ $test == false ]; then
    echo -e 'Command not found.\nTry "./init.sh help" for more information.'
fi
